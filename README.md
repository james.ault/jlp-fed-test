# UI Dev Technical Test - Dishwasher App

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

We have started the project, but we'd like you to finish it off to a production-ready standard. Bits of it may be broken.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy. 

For this exercise we’d be looking at only displaying the first 20 results returned by the API.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

### Mock Data

There is mock data available for testing in the `mockData` folder.

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Jest setup.
- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md.
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the NPM dependencies using `npm i`. (You will need to have already installed Node.js using version `14.x`.)
- Run the development server with `npm run dev`
- Open [http://localhost:3001](http://localhost:3001) with your browser.

## Assumptions
- The major versions for the implemented core frameworks (React / Next / Node) should remain the same as the instruction is to finish off the app rather than rebuild it.
- The production build for the project should not contain any critical or high risk dependancies
- The development build for the project should not contain any high risk dependancies
## Plan

- Fork, clone and install project to local machine
### Assess Project Initial Setup
- Assess npm packages/audit results
- Assess existing components
- Assess existing testing/linting setup
- Assess page structures
- Assess target requirements
- Run the dev server as-is

### Fix
- Address issues found from assessment
- Start using mocked data for dev work rather than calling live APIs
- Amend testing configuration to allow use of React Testing Library
- Start adding unit tests for components
- Implement routing to 404 component

### Enhance
- Break up existing pages into components
- start adding styles to pages and components
- begin considering supplied design form factors
- add more robust error handling to the app that should cater for more than 404 status codes. This would include building an alternate set of error pages.

### Future Work
- Continue progressing through product detail page components and styling
- Implement image carousel component instead of current single image display
- Convert product specification from list items into a table structure
- Complete portrait mobile page structure
- Implement landscape/desktop page layout and styling
- Reimplement and test integration with live product data apis

## Notes
### Remaining Security Vulnerabilities
NEXT.js v10 contains all of the remaining NPM vulnerabilities in the project, I have made the asssumption that next.js 10 is the major version of next currently in use at JLP and therefore I will not be upgrading it as part of this project.
### Font Weights
Currently there are only two font weights available for the custom font GilSansForJL (200/400). In order to meet the font weights shown in the example designs it would be required to import more fonts. In the interests of time I have used the existing font weights across the project rather than investigating the additional versions of this custom font.

### Accuracy compared to design images
It has been quite difficult to estimate the colours, font sizes and spacing when trying to match against the design images. If I were working in a team on this project I would look to aquire some more detailed mockups that would make it easier to assess the direct requirements for the design rather than making guesses by eye.
## Enhancement Suggestions
### NEXT.js 10 Upgrade
In line with the note made above regarding the vulnerabilities present in NEXT.js 10 I would recommend upgrading the major version to the latest stable version (v12.2.3 at time of writing).
### Font Weights - Additional Font Files or a Custom Variable Font
As noted above the app currently only has access to two font weights for the custom font family. In order to make more font-weights available I would recommend investigating any other font-weight files that are available for GilSansForJL or investing in a custom variable font to allow for more flexibility of font-weight without the need to import multiple font files.

### Image Carousel
Of the 'future work' that I have outlined I believe that the introduction of a fully functioning image carousel will need the most consideration. Without detailed UX/UI requirements there are a number of approaches that could be taken which would need to have the pros and cons weighed up before moving forward. A 3rd Party component could be assessed to save time however it is often the case that bringing in external packages can bring in additional complexity or dependancy vulnerabilities compared to undergoing the work to build our own implementation. 