import Layout from "../src/components/layout/layout";
import "../src/styles/globals.scss";
import React from "react";
import { PropTypes } from 'prop-types';

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

MyApp.propTypes = {
  Component: PropTypes.func,
  pageProps: PropTypes.object,
}

export default MyApp;
