import ProductCarousel from '../../src/components/product-carousel/product-carousel';
import mockDetailsData from '../../mockData/data2.json';
import React from 'react';
import { PropTypes } from 'prop-types';
import { ProductHeader } from '../../src/components/product-header/product-header';
import { ProductPriceInfo } from '../../src/components/product-price-info/product-price-info';
import { ProductInformation } from '../../src/components/product-information/product-information';
import { ProductSpecification } from '../../src/components/product-specification/product-specification';
import NotFound from '../404';
import styles from './product-detail.module.scss'

export async function getServerSideProps(context) {
  const id = context.params.id;
  // const response = await fetch(
  //   "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
  // );
  // const data = await response.json();
  const data = mockDetailsData.detailsData.find(
    (product) => product.productId === id
  );

  if (!data) {
    return {
      props: {
        error: true,
      },
    };
  }

  return {
    props: { data },
  };
}

const ProductDetail = ({ data, error }) => {
  if (error) {
    return <NotFound />
  }

  return (
    <div className={styles.productDetail}>
      <ProductHeader title={data.title} />
      <div>
        <ProductCarousel image={data.media.images.urls[0]} />

        <ProductPriceInfo
          price={data.price}
          displaySpecialOffer={data.displaySpecialOffer}
          additionalServices={data.additionalServices}
        />

        <ProductInformation productInformation={data.details.productInformation}  />
        <ProductSpecification
          attributes={data.details.features[0].attributes}
        />
      </div>
    </div>
  );
};

ProductDetail.propTypes = {
  // Product Details returned from product api
  data: PropTypes.object,
  error: PropTypes.bool,
};

export default ProductDetail;
