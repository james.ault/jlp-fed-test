import Head from 'next/head';
import mockListData from '../mockData/data.json';
import ProductGrid from '../src/components/product-grid/product-grid';
import React from 'react';
import PropTypes from 'prop-types';
import styles from './index.module.scss';
import NotFound from './404';

export async function getServerSideProps() {
  // const response = await fetch(
  //   "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
  // );
  // const data = await response.json();
  const data = mockListData;

  if (!data) {
    return {
      props: {
        error: true,
      },
    };
  }
  return {
    props: {
      data,
    },
  };
}

const Home = ({ data, error }) => {
  if (error) {
    return <NotFound />;
  }
  const productCount = data.products.length;
  const items = data.products.slice(0, 20);
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1 className={styles['page-title']}>
          Dishwashers {`(${productCount})`}
        </h1>
        <ProductGrid items={items} />
      </div>
    </div>
  );
};

Home.propTypes = {
  // Product Listing data returned from api
  data: PropTypes.object,
  error: PropTypes.bool,
};

export default Home;
