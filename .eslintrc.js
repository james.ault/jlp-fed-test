module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'standard',
    'prettier'
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: [
    'react'
  ],
  rules: {
  },
  settings: {
    react: {
      version: 'detect',
    },
    'import/extensions': ['.js', '.jsx'],
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx'],
      },
    },
  },
  ignorePatterns: ['.next', 'node_modules'],
  overrides: [
    {
      plugins: ['testing-library', 'jest-dom'],
      extends: [
        'plugin:jest-dom/recommended',
        'plugin:testing-library/react',
      ],
      files: ['*.test.jsx'],
      env: {
        jest: true,
      },
    },
  ],
}
