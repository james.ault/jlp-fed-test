import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import ProductGrid from './product-grid';
import mockListData from '../../../mockData/data.json';

describe('ProductGrid', () => {
  const props = {
    items: mockListData.products.slice(0, 5),
  };
  it('renders 5 ProductListItem components', () => {
    render(<ProductGrid {...props} />);
    expect(screen.getAllByRole('link')).toHaveLength(5);
  });
});
