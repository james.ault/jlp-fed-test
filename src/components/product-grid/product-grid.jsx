import React from 'react';
import ProductListItem from '../product-list-item/product-list-item';
import Link from 'next/link';
import styles from './product-grid.module.scss';
import { PropTypes } from 'prop-types';

const ProductGrid = ({ items }) => {
  return (
    <div className={styles.content}>
      {items.map((item) => (
        <Link
          key={item.productId}
          href={{
            pathname: '/product-detail/[id]',
            query: { id: item.productId },
          }}
        >
          <a className={styles.link}>
            <ProductListItem
              image={item.image}
              price={item.price.now}
              title={item.title}
            />
          </a>
        </Link>
      ))}
    </div>
  );
};

ProductGrid.propTypes = {
  // Product List item data
  items: PropTypes.array,
};

export default ProductGrid;
