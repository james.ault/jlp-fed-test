import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { ProductInformation } from './product-information';

describe('ProductInformation', () => {
  const props = {
    productInformation: "<p data-testid='child-element'>Boasting plenty of convenient features within a simple-to-use design, the SMS24AW01G Freestanding Dishwasher from Bosch is sure to be a welcome helping hand in any kitchen.</pThis useful feature makes sure your detergent is completely dissolved for a more effective wash by releasing it into a special tray on top of the basket and mixing it into the cyBosch's revolutionary brushless motor has been developed to achieve maximum power with minimum energy loss. It's now quieter, faster, more energy efficient, durable and powerful.The SMS24AW01G offers total flexibility with its varioBasket system. The top basket can be adjusted between 3 height positions and both top and bottom drawers contain plenty of r<li>Quick 45 °C wash</li></ul></p>trong><br>",
  };

  it('renders nothing if no props are provided', () => {
    const { container } = render(<ProductInformation />);
    expect(container).toBeEmptyDOMElement();
  });

  it('renders the provided productInformation', () => {
    render(<ProductInformation {...props} />);
    expect(screen.getByTestId('child-element')).toBeInTheDocument();
  });
});