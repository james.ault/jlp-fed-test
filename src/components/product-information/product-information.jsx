import React from 'react';
import { PropTypes } from 'prop-types';
import styles from './product-information.module.scss'

export const ProductInformation = ({ productInformation }) => {
  if (!productInformation) {
    return null;
  }
  return (
    <div className={styles.productInformation}>
      <h3>Product Information</h3>
      <div dangerouslySetInnerHTML={{__html: productInformation}} />
    </div>
  );
};

ProductInformation.propTypes = {
  // Product information
  productInformation: PropTypes.string,
};
