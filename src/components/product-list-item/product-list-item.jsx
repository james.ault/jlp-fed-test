import React from 'react';
import styles from './product-list-item.module.scss';
import { PropTypes } from 'prop-types';

const ProductListItem = ({ image, price, title }) => {
  return (
    <div className={styles['product-list-item']}>
      <div>
        <img src={image} alt="" className={styles['product-list-item-img']} />
      </div>
      <div>{title}</div>
      <div className={styles['product-list-item-price']}>£{price}</div>
    </div>
  );
};

ProductListItem.propTypes = {
  // Product image url
  image: PropTypes.string,
  // Product Price
  price: PropTypes.string,
  // Product Title
  title: PropTypes.string,
};

export default ProductListItem;
