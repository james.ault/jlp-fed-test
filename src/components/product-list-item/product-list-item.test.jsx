import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import ProductListItem from './product-list-item';

describe('ProductListItem', () => {
  const props = {
    image: '//johnlewis.scene7.com/is/image/JohnLewis/234378764?',
    title: 'product title',
    price: '100',
  };
  it('renders the provided product image, title and price', () => {
    render(<ProductListItem {...props} />);
    expect(screen.getByText('£100')).toBeInTheDocument();
    expect(screen.getByText('product title')).toBeInTheDocument();
    expect(screen.getByRole('img')).toHaveAttribute(
      'src',
      '//johnlewis.scene7.com/is/image/JohnLewis/234378764?'
    );
  });
});
