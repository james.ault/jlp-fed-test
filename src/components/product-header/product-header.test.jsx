import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { ProductHeader } from './product-header';

describe('ProductHeader', () => {
  const props = {
    title: 'product title',
  };
  it('renders the provided product title', () => {
    render(<ProductHeader {...props} />);
    expect(screen.getByText('product title')).toBeInTheDocument();
  });
});
