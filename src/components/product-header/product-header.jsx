import React from 'react';
import { PropTypes } from 'prop-types';
import styles from './product-header.module.scss';

export const ProductHeader = ({ title }) => {
  return <h1 className={styles['product-header-title']}>{title}</h1>;
};

ProductHeader.propTypes = {
  // Product Details returned from product api
  title: PropTypes.string,
};
