import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { ProductPriceInfo } from './product-price-info';

describe('ProductPriceInfo', () => {
  const props = {
    price: { now: '100' },
    displaySpecialOffer: '2 YEAR GUARANTEE INCLUDED',
    additionalServices: { includedServices: ['2 year guarantee included'] },
  };

  it('renders nothing if no props are provided', () => {
    const { container } = render(<ProductPriceInfo />);
    expect(container).toBeEmptyDOMElement();
  });

  it('renders the provided price, displaySpecialOffer and included services', () => {
    render(<ProductPriceInfo {...props} />);
    expect(screen.getByText('£100')).toBeInTheDocument();
    expect(screen.getByText('2 YEAR GUARANTEE INCLUDED')).toBeInTheDocument();
    expect(screen.getByText('2 year guarantee included')).toBeInTheDocument();
  });
});
