import React from 'react';
import { PropTypes } from 'prop-types';
import { AdditionalServices } from '../additional-services/additional-services';
import styles from './product-price-info.module.scss'

export const ProductPriceInfo = ({
  price,
  displaySpecialOffer,
  additionalServices,
}) => {
  if (!price && !displaySpecialOffer && !additionalServices) {
    return null;
  }
  return (
    <div className={styles.productPriceInfo}>
      {price?.now && <div className={styles.price}>£{price.now}</div>}
      {displaySpecialOffer && <div className={styles.specialOffer}>{displaySpecialOffer}</div>}
      <AdditionalServices
        includedServices={additionalServices.includedServices}
      />
    </div>
  );
};

ProductPriceInfo.propTypes = {
  // Product Details returned from product api
  price: PropTypes.object,
  displaySpecialOffer: PropTypes.string,
  additionalServices: PropTypes.object,
};
