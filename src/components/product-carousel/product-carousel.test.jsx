import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import ProductCarousel from './product-carousel';

describe('ProductListItem', () => {
  const props = {
    image: '//johnlewis.scene7.com/is/image/JohnLewis/234378764?',
  };
  it('renders the provided product image, title and price', () => {
    render(<ProductCarousel {...props} />);

    expect(screen.getByRole('img')).toHaveAttribute(
      'src',
      '//johnlewis.scene7.com/is/image/JohnLewis/234378764?'
    );
  });
});
