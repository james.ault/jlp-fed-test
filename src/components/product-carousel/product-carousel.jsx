import React from 'react';
import styles from './product-carousel.module.scss';
import { PropTypes } from 'prop-types';

const ProductCarousel = ({ image }) => {
  return (
    <div className={styles.productCarousel} >
      <img src={image} alt="" style={{ width: '100%', maxWidth: '500px' }} />
    </div>
  );
};

ProductCarousel.propTypes = {
  // Product Image Url
  image: PropTypes.string,
};

export default ProductCarousel;
