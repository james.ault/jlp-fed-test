import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { AdditionalServices } from './additional-services';

describe('AdditionalServices', () => {
  const props = {
    includedServices: ['2 year guarantee included'],
  };

  it('renders nothing if no props are provided', () => {
    const { container } = render(<AdditionalServices />);
    expect(container).toBeEmptyDOMElement();
  });

  it('renders the provided includedServices', () => {
    render(<AdditionalServices {...props} />);
    expect(screen.getByText('2 year guarantee included')).toBeInTheDocument();
  });
});
