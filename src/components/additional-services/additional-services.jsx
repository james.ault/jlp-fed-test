import React from 'react';
import { PropTypes } from 'prop-types';

export const AdditionalServices = ({ includedServices }) => {
  if (!includedServices) {
    return null;
  }
  return includedServices.map((service) => {
    return <div key={service}>{service}</div>;
  });
};

AdditionalServices.propTypes = {
  includedServices: PropTypes.array,
};
