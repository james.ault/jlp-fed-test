import React from 'react';
import { PropTypes } from 'prop-types';

export const ProductSpecification = ({ attributes }) => {
  if (!attributes) {
    return null;
  }
  return (
    <>
      <h3>Product specification</h3>
      <ul>
        {attributes.map((item) => (
          <li key={item.name}>
            <div>
              <div>{item.name}</div>
            </div>
          </li>
        ))}
      </ul>
    </>
  );
};

ProductSpecification.propTypes = {
  attributes: PropTypes.array,
};
