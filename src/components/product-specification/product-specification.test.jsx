import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { ProductSpecification } from './product-specification';

describe('ProductSpecification', () => {
  const props = {
    attributes: [
      {
        value: 'Integrated',
        values: [],
        multivalued: false,
        id: 'attr80000016332',
        name: 'Type',
        toolTip: '',
        uom: '',
      },
      {
        value: '50dB',
        values: [],
        multivalued: false,
        id: 'attr80000016333',
        name: 'Noise Level',
        toolTip:
          'How noisy, in decibels, the machine is. The lower the dB, the quieter the appliance. A human voice in normal conversation is approximately 60dB, while an alarm clock might be 80dB',
        uom: '',
      },
    ],
  };

  it('renders nothing if no props are provided', () => {
    const { container } = render(<ProductSpecification />);
    expect(container).toBeEmptyDOMElement();
  });

  it('renders the provided product attributes', () => {
    render(<ProductSpecification {...props} />);
    expect(screen.getByText('Type')).toBeInTheDocument();
  });
});
