import React from 'react';
import styles from './layout.module.scss';
import { PropTypes } from 'prop-types';

const Layout = ({ children }) => {
  return (
    <div className={styles.content}>
      <main className={styles.main}>
        <div>
          <div>{children}</div>
        </div>
      </main>
    </div>
  );
};

Layout.propTypes = {
  // wrapped contents of the layout component
  children: PropTypes.object,
};

export default Layout;
