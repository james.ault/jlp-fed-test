import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Layout from './layout';

describe('Layout', () => {
  const children = (<div>test div</div>);

  it('renders the child elements in the layout', () => {
    render(<Layout>{children}</Layout>);
    expect(screen.getByText('test div')).toBeInTheDocument();
  });
});
